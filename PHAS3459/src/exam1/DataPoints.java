package exam1;

class DataPoints {
	
	protected String Player, Team, Pos;
	protected double AVG, OBP;
	protected int G, AB, R, H, H2, H3, HR, RBI;
	

	//constructor
	public DataPoints(String cPlayer, String cTeam, String cPos, int cG, int cAB,
			int cR, int cH, int cH2, int cH3, int cHR, int cRBI, double cAVG, double cOBP
			){
		Player=cPlayer;
		Team=cTeam;
		Pos=cPos;
		G = cG;
		AB = cAB;
		R = cR;
		H = cH;
		H2 = cH2;
		H3 = cH3;
		HR = cHR;
		RBI = cRBI;
		AVG = cAVG;
		OBP = cOBP;
	}

	//get methods 
	public String getPlayer() {
		return Player;
	}
	public String getTeam() {
		return Team;
	}
	public String getPos() {
		return Pos;
	}
	
	public int getG() {
		return G;
	}
	public int getAB() {
		return AB;
	}
	public int getR() {
		return R;
	}
	public int getH() {
		return H;
	}
	public int getH2() {
		return H2;
	}
	public int getH3() {
		return H3;
	}
	public int getHR() {
		return HR;
	}
	public int getRBI() {
		return RBI;
	}
	public double getAVG() {
		return AVG;
	}
	public double getOBP() {
		return OBP;
	}

}

