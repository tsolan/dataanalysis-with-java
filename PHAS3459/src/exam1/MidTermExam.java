package exam1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;



public class MidTermExam {
	
	

	public static void main(String[] args) {
		String url = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/MLB2001Hitting.txt";
		ArrayList<DataPoints> data = new ArrayList<DataPoints>();
		data = dataFromURL(url);
		System.out.print("Number of players: ");getNumber(data);
		System.out.print("Player with highest HRs: ");maxHR(data);
		System.out.println();
		Teams(data);

	}
	//method obtaining and storing data from our url
	public static ArrayList<DataPoints> dataFromURL(String url){
		ArrayList<DataPoints> tokens = new ArrayList<DataPoints>();
		try{
			URL u = new URL(url);
			InputStream inputstream = u.openStream();
			InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
			BufferedReader br = new BufferedReader(inputstreamreader);
			@SuppressWarnings("resource")
			Scanner s = new Scanner(br).useDelimiter("\t");
			s.nextLine(); //since first 2 lines contain no relevant data
			s.nextLine();
			while(s.hasNextLine()){
						String[] parts = s.nextLine().split("\t");
						tokens.add(new DataPoints(
								parts[0],
								parts[1],
								parts[2],
								Integer.parseInt(parts[3]),
								Integer.parseInt(parts[4]),
								Integer.parseInt(parts[5]),
								Integer.parseInt(parts[6]),
								Integer.parseInt(parts[7]),
								Integer.parseInt(parts[8]),
								Integer.parseInt(parts[9]),
								Integer.parseInt(parts[10]),
								Double.parseDouble(parts[11]),
								Double.parseDouble(parts[12])));
					}
				
				
		}
		//catching exceptions
		catch(FileNotFoundException e){
			System.out.println("Error: File not found: "+e.getMessage());		
		}
		catch(IOException e){
			System.out.println("Error: "+e.getMessage());
		}
		return tokens;
	}
	
	//method getting number of all players
	public static void getNumber(ArrayList<DataPoints> data){
		Iterator<DataPoints> it = data.iterator();
		int num = 0;
		while (it.hasNext()){
			
			num++;
			
			}			
		System.out.println(num-1); //since there is  one extra line in the end
	}
	//method getting the name of the player with highest number of HRs
	public static void maxHR(ArrayList<DataPoints> data){
		Iterator<DataPoints> it = data.iterator();
		int num = 0;
		String bestPlayer=null;
		while (it.hasNext()){
			DataPoints line = it.next();
			int HR = line.getHR();
			String Player = line.getPlayer();
			if (HR>=num){
				num=HR;
				bestPlayer = Player;
				
			}
		}
		System.out.println(bestPlayer);
	}
	//getting number of players with 10 or more ABs for each team
	public static void Teams(ArrayList<DataPoints> data){
		Iterator<DataPoints> it = data.iterator();
		ArrayList<String> teamnames = new ArrayList<String>();
		int num = 0;
		while (it.hasNext()){
			DataPoints line = it.next();
			if  (!teamnames.contains(line.getTeam())){
				teamnames.add(line.getTeam());
			}
			for(int i=0;i<=teamnames.size();i++){
				if (line.getTeam().equals(teamnames.get(i))){
					if (line.getAB()>=10){
						num++;
					}
					System.out.println("Team: "+line.getTeam()+ " Number of players with AB>=10: "+ num);
				}
			}


		}
	}
	

}