package exam2;

import java.util.Collection;
/**
 *  Classify sounds by duration
 * @author AnilPanda
 *
 */
public class ClassifyByDuration  implements ClassifySound{
		
	/**
	 * returns string based on the duration of sound
	 */
	public String classifySound(Recording data, Collection<Integer> sample_data){
		
		double duration = data.number/Double.valueOf(data.frequency);
		
		//checking if the duration of the sound is more than or less than 1 second and return the appropriate string
		if (duration > 1.0){
			return "long";
		}
		else{
			return "short";
		}
	}

}
