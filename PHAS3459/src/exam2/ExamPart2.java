package exam2;

import java.util.Collection;
/**
 * Classify sound from the urls by duration and amplitude
 * @author Anil Panda
 *
 */
public class ExamPart2 {

	public static void main(String[] args) {
		
		        //urls of the instrument names 
				String instURL = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/index.txt";
				
				//urls of instrument recordings 
				String rec1 = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/recording01.txt";
				String rec2 = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/recording02.txt";
				String rec3 = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/recording03.txt";
				String rec4 = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/recording04.txt";
				
				//urls of software recordings 
				String genA = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/genA.txt";
				String genB = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/genB.txt";
				String genC = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/genC.txt";
				
				String spaceSplitter = "\\s+"; // Regex for any number of whitespaces

				try{			
					System.out.println("PART 2");
					
					//creating Recording variables from data using functions designed
					Recording record1 = Input.importRecording(rec1, spaceSplitter);
					Recording record2 = Input.importRecording(rec2, spaceSplitter);
					Recording record3 = Input.importRecording(rec3, spaceSplitter);
					Recording record4 = Input.importRecording(rec4, spaceSplitter);
					
					Recording record5 = Input.importRecording(genA, spaceSplitter);
					Recording record6 = Input.importRecording(genB, spaceSplitter);
					Recording record7 = Input.importRecording(genC, spaceSplitter);
					
					//Obtaining the Collection of sample amplitudes
					Collection<Integer> sample_rec1 = Input.importSample(rec1, spaceSplitter);
					Collection<Integer> sample_rec2 = Input.importSample(rec2, spaceSplitter);
					Collection<Integer> sample_rec3 = Input.importSample(rec3, spaceSplitter);
					Collection<Integer> sample_rec4 = Input.importSample(rec4, spaceSplitter);
					
					Collection<Integer> sample_rec5 = Input.importSample(genA, spaceSplitter);
					Collection<Integer> sample_rec6 = Input.importSample(genB, spaceSplitter);
					Collection<Integer> sample_rec7 = Input.importSample(genC, spaceSplitter);
					
					//Create objects, which methods are used to classify sounds
					ClassifyByDuration cs = new ClassifyByDuration();
					ClassifyByAmplitude ca = new ClassifyByAmplitude();
					
					//Use methods designed to classify sounds for all of the recordings obtained
					String duration1 = cs.classifySound(record1, sample_rec1);
					String amplitude1 = ca.classifySound(record1, sample_rec1);
					System.out.println("Classification by duration of recording 1: " + duration1);
					System.out.println("Classification by amlitude of recording 1: " + amplitude1);
					System.out.println();
					
					String duration2 = cs.classifySound(record2, sample_rec2);
					String amplitude2 = ca.classifySound(record2, sample_rec2);
					System.out.println("Classification by duration of recording 2: " + duration2);
					System.out.println("Classification by amlitude of recording 2: " + amplitude2);
					System.out.println();
					
					String duration3 = cs.classifySound(record3, sample_rec3);
					String amplitude3 = ca.classifySound(record3, sample_rec3);
					System.out.println("Classification by duration of recording 3: " + duration3);
					System.out.println("Classification by amlitude of recording 3: " + amplitude3);
					System.out.println();

					String duration4 = cs.classifySound(record4, sample_rec4);
					String amplitude4 = ca.classifySound(record4, sample_rec4);
					System.out.println("Classification by duration of recording 4: " + duration4);
					System.out.println("Classification by amlitude of recording 4: " + amplitude4);
					System.out.println();

					String duration5 = cs.classifySound(record5, sample_rec5);
					String amplitude5 = ca.classifySound(record5, sample_rec5);
					System.out.println("Classification by duration of genA: " + duration5);
					System.out.println("Classification by amlitude of genA: " + amplitude5);
					System.out.println();

					String duration6 = cs.classifySound(record6, sample_rec6);
					String amplitude6 = ca.classifySound(record6, sample_rec6);
					System.out.println("Classification by duration of genB: " + duration6);
					System.out.println("Classification by amlitude of genB: " + amplitude6);
					System.out.println();

					String duration7 = cs.classifySound(record7, sample_rec7);
					String amplitude7 = ca.classifySound(record7, sample_rec7);
					System.out.println("Classification by duration of genC: " + duration7);
					System.out.println("Classification by amlitude of genC: " + amplitude7);

	}
				//catch exceptions
				catch (Exception e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
	}

	}
}
