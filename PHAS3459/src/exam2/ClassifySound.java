package exam2;

import java.util.Collection;
/**
 * Sound classifying interface
 * @author Anil Panda
 *
 */
public interface ClassifySound {

	/**
	 * Initializing the method for sound classification
	 * @param data
	 * @param sample_data
	 * @return the String of classification of the sound
	 */
	String classifySound(Recording data, Collection<Integer> sample_data);
}
