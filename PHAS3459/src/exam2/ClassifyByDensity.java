package exam2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * Classify sounds by spectral density
 * @author Anil Panda
 *
 */
public class ClassifyByDensity implements ClassifySound {

	/**
	 * returns Strings based on the biggest spectral density from the range of given frequencies
	 */
	public String classifySound(Recording data, Collection<Integer> sample_data) {
		
		//The function gets Collection as input, so it has to be converted to ArrayList in order to interact with spectralDensity function
		 Collection<Integer> hashset = new HashSet<Integer>(sample_data);
		 ArrayList<Integer> sample_data1 = new ArrayList<Integer>(hashset);
		 
		 
		 double duration = data.number/Double.valueOf(data.frequency); //obtain double value of duration

		 //obtain spectral density for 100, 400 and 1000 Hz
		 double hz_100 = spectralDensity(sample_data1, duration, 100.0);
		 double hz_400 = spectralDensity(sample_data1, duration, 400.0);
		 double hz_1000 = spectralDensity(sample_data1, duration, 1000.0);
		
		 //compare the obtained densities and return an appropriate string
		 if (hz_100>hz_400 & hz_100>hz_1000){
			 return "low";
		 }
		 if (hz_400>hz_100 & hz_400>hz_1000){
			 return "medium";
		 }
		 if (hz_1000>hz_100 & hz_1000>hz_400){
			 return "high";
		 }
		 else return "Some of the densities are equal";
	}
	

	/**
	 * spectral density calculation method modified to use ArrayList of samples instead of long[]
	 * @param samples
	 * @param duration
	 * @param frequency
	 * @return Spectral Density
	 */
	private double spectralDensity(ArrayList<Integer> samples, double duration, double frequency) {
		//Use ArrayList, because previous method designed to use Collections rather than arrays
		int bigN = samples.size();
		double z = 2 * Math.PI * frequency * duration / bigN;
		double sumCos = 0;
		double sumSin = 0;
		for (int n = 0; n < bigN; ++n) {
		sumCos += samples.get(n)* Math.cos(z*n);
		sumSin += samples.get(n)* Math.sin(z*n);
		}
		double norm = duration / (bigN*bigN);
		return norm * (sumCos*sumCos + sumSin*sumSin);
		}
}
