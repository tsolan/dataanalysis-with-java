package exam2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class used to obtain the data from the URLs and save it to appropriate formats
 * @author Anil Panda
 *
 */
public class Input {

	/** 
 	 * Scanner from URL 
 	 * @param url Location of text file containing data 
 	 * @return Scanner  
 	 * @throws IOException 
 	 */ 
 	private static Scanner scFromURL(String url) throws IOException { 
 		URL u = new URL(url); 
 		Scanner sc = new Scanner(new BufferedReader(new InputStreamReader(u.openStream()))); 
 		return sc; 
 	} 
 	
 	/**
 	 * Returns Recording object from the url
 	 * @param url
 	 * @param splitter
 	 * @return Recording object
 	 * @throws IOException
 	 */
 	public static Recording importRecording(String url, String splitter) throws IOException{
 		
 		Scanner sc = scFromURL(url);
		String line;
		String[] lineArray;
		line = sc.nextLine();
		lineArray = line.split(splitter);
		Recording rec = createRecording(lineArray);
		
 		return rec;
 	}
 	/**
 	 * creates recording object from given array of strings
 	 * @param lineArray
 	 * @return Recording object
 	 */
 	public static Recording createRecording(String[] lineArray){
 		int n_inputElements = 3; // expected no. elements  
 		
 		if (lineArray.length != n_inputElements) { // Check if input contains expected number of elements 
 			throw new InputMismatchException("ERROR: Input does not contain "+
 		n_inputElements+
 		" elements! Recording data must only contain: frequency, number oif samples and max Amplitude."); 
 			
 		} 
 		else { 
 			int frequency = Integer.valueOf(lineArray[0]);
 			int number = Integer.valueOf(lineArray[1]);
 			int maxAmp = Integer.valueOf(lineArray[2]);
 			Recording r = new Recording(frequency, number, maxAmp);
 			return r;
 		}
 	}
 	/**
 	 * Get the integer collection of sample amplitudes from URL
 	 * @param url
 	 * @param splitter
 	 * @return Collection of int sample amplitudes
 	 * @throws IOException
 	 */
 	public static Collection<Integer> importSample(String url, String splitter) throws IOException{
 		Collection<Integer> samplesData = new ArrayList<Integer>();
 		Scanner sc = scFromURL(url); 
 		sc.nextLine();
 		while (sc.hasNextLine()) { 
 			String line; 
 			line = sc.nextLine(); 
 			int sample = Integer.valueOf(line);
 			samplesData.add(sample);
 	}
 		sc.close(); // Close scanner to prevent memory leak 
 		return samplesData;
 	}
 	/**
 	 * creates Instrument object from array of strings
 	 * @param lineArray
 	 * @return Instrument object
 	 */
 	public static Instrument createInstrument(String[] lineArray) { 
 		int n_inputElements = 2; // expected no. elements 
 		if (lineArray.length != n_inputElements) { // Check if input contains expected number of elements 
 			throw new InputMismatchException(
 					"ERROR: Input does not contain "+n_inputElements+
 					" elements! Insturment data must only contain: filename and instrument name"); 
 		} 
 		else { 
 			String filename = lineArray[0]; 
 			String instrument = lineArray[1]; 
 			Instrument s = new Instrument(filename, instrument); // Instantiate Instrument with inputs 
 			return s; 
 		} 
 		
 	}
 	/**
 	 * saves the data from URL to collection of Instrument objects
 	 * @param url
 	 * @param splitter
 	 * @return Collection of Instruments
 	 * @throws IOException
 	 */
 	public static Collection<Instrument> importInstruments (String url, String splitter) throws IOException { 
 		Collection<Instrument> IList = new ArrayList<Instrument>(); 
 		Scanner sc = scFromURL(url); 
 		while (sc.hasNextLine()) { 
 			String line; 
 			String[] lineArray; 
 			line = sc.nextLine();
 			lineArray = line.split(splitter); // Split current line by splitter 
 			Instrument s = createInstrument(lineArray); 
 			IList.add(s); 
 		} 
 		sc.close(); // Close scanner to prevent memory leak 
 		return IList; 
 	} 

 	


 	
 	
 		
 		
 		

 	
 	

 	
}
