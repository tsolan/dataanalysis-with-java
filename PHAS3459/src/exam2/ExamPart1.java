package exam2;

import java.util.Collection;
import java.util.Iterator;

/**
 * Obtaining filename, duration, RMS and instrument for given data
 * @author Anil Panda
 *
 */
public class ExamPart1 {

	public static void main(String[] args) {
		
		//urls of the instrument names
		String instURL = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/index.txt";
		
		//urls of instrument recordings 
		String rec1 = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/recording01.txt";
		String rec2 = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/recording02.txt";
		String rec3 = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/recording03.txt";
		String rec4 = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/recording04.txt";
		
		//urls of software recordings
		String genA = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/genA.txt";
		String genB = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/genB.txt";
		String genC = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/genC.txt";
		
		String spaceSplitter = "\\s+"; // Regex for any number of whitespaces

		try{			
			System.out.println("PART 1");
			
			//creating Recording variables from data using functions designed
			Recording record1 = Input.importRecording(rec1, spaceSplitter);
			Recording record2 = Input.importRecording(rec2, spaceSplitter);
			Recording record3 = Input.importRecording(rec3, spaceSplitter);
			Recording record4 = Input.importRecording(rec4, spaceSplitter);
			
			Recording record5 = Input.importRecording(genA, spaceSplitter);
			Recording record6 = Input.importRecording(genB, spaceSplitter);
			Recording record7 = Input.importRecording(genC, spaceSplitter);
			
			//Obtaining the duration
			double dur_rec1 = record1.number/Double.valueOf(record1.frequency); //need to convert at least one of the variables
			double dur_rec2 = record2.number/Double.valueOf(record2.frequency); // to double to apply proper division
			double dur_rec3 = record3.number/Double.valueOf(record3.frequency);
			double dur_rec4 = record4.number/Double.valueOf(record4.frequency);
			
			double dur_rec5 = record5.number/Double.valueOf(record5.frequency);
			double dur_rec6 = record6.number/Double.valueOf(record6.frequency);
			double dur_rec7 = record7.number/Double.valueOf(record7.frequency);
			
			//Obtaining the Collection of sample amplitudes
			Collection<Integer> sample_rec1 = Input.importSample(rec1, spaceSplitter);
			Collection<Integer> sample_rec2 = Input.importSample(rec2, spaceSplitter);
			Collection<Integer> sample_rec3 = Input.importSample(rec3, spaceSplitter);
			Collection<Integer> sample_rec4 = Input.importSample(rec4, spaceSplitter);
			
			Collection<Integer> sample_rec5 = Input.importSample(genA, spaceSplitter);
			Collection<Integer> sample_rec6 = Input.importSample(genB, spaceSplitter);
			Collection<Integer> sample_rec7 = Input.importSample(genC, spaceSplitter);
			
			//Printing the obtained data
			System.out.println("Filename: recording01.txt, Duration: " + dur_rec1 +
					", RMS="+ 20*Math.log10((RMS(sample_rec1)/record1.maxAmp))
					+ ", Instrument: oboe" );
			System.out.println("Filename: recording02.txt, Duration: " + dur_rec2 + 
					", RMS="+ 20*Math.log10((RMS(sample_rec2)/record2.maxAmp))
					+ ", Instrument: cello");
			System.out.println("Filename: recording03.txt, Duration: " + dur_rec3 + 
					", RMS="+ 20*Math.log10((RMS(sample_rec3)/record3.maxAmp))
					+ ", Instrument: bassoon");
			System.out.println("Filename: recording04.txt, Duration: " + dur_rec4 +
					", RMS="+ 20*Math.log10((RMS(sample_rec4)/record4.maxAmp))
					+ ", Instrument: violin");
			
			System.out.println("Filename: genA.txt, Duration: " + dur_rec5 +
					", RMS="+ 20*Math.log10((RMS(sample_rec5)/record5.maxAmp))
					+ ", Instrument: software");
			
			System.out.println("Filename: genB.txt, Duration: " + dur_rec6 +
					", RMS="+ 20*Math.log10((RMS(sample_rec6)/record6.maxAmp))
					+ ", Instrument: software");
			
			System.out.println("Filename: genC.txt, Duration: " + dur_rec7 +
					", RMS="+ 20*Math.log10((RMS(sample_rec7)/record7.maxAmp))
					+ ", Instrument: software");			
			}	
		//catch exceptions
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
            }
	}
	
	/**
	 * Method obtained the sum of the squares of all of the sample amplitudes
	 * @param sData
	 * @return
	 */
	public static double RMS(Collection<Integer> sData){
		Iterator<Integer> sItr = sData.iterator(); //iterating over sample collection
		double rms  = 0;
		while (sItr.hasNext()){
			int sample = sItr.next();
			rms+=(sample*sample); //sum of squares 
		}
		return Math.sqrt(rms); //returns RMS of the data


	}

}
