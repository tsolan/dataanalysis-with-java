package exam2;

import java.util.ArrayList;
import java.util.Collection;
/**
 * Methods for Recording object created
 * @author AnilPanda
 *
 */
public class Recording {

	//variables needed
	final int frequency;
	final int number;
	final int maxAmp;

	
	private Collection<Integer> samples = new ArrayList<Integer>();
	
/**
 * Constructor
 * @param frequency
 * @param number
 * @param maxAmp
 */
	public Recording(int frequency, int number, int maxAmp) {
		this.frequency = frequency;
		this.number = number;
		this.maxAmp = maxAmp;
	}

	/**
	 * getter for samples
	 * @return samples data
	 */
	public Collection<Integer> getRecords() {
		return samples;
	}


	/**
	 * method for nice representation
	 */
	@Override
	public String toString() {
		return "Recording [Frequency: " + frequency + ", number of samples: " + number + ", maximum amplitude=" + maxAmp + "]";
	}
}
