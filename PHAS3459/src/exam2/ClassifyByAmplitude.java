package exam2;

import java.util.Collection;
import java.util.Iterator;
/**
 * Classify sound by Amplitude
 * @author AnilPanda
 *
 */
public class ClassifyByAmplitude implements ClassifySound {

	/**
	 * returns string based on the amplitude value
	 */
	public String classifySound(Recording data, Collection<Integer> sample_data) {
		
		//obtain the amplitude using RMS function and formula given
		double amplitude = 20*Math.log10((RMS(sample_data)/data.maxAmp));
		
		//check if amplitude more than or less than -20dBFS and returns appropriate String
		if (amplitude>(-20.0)){
			return "loud";
		}
		else{
			return "quiet";
		}
	}
	
	/**
	 * Method obtained the sum of the squares of all of the sample amplitudes
	 * @param sData
	 * @return
	 */
	public static double RMS(Collection<Integer> sData){
		Iterator<Integer> sItr = sData.iterator();
		double rms  = 0;
		while (sItr.hasNext()){
			int sample = sItr.next();
			rms+=(sample*sample);
		}
		return Math.sqrt(rms);
		}

}
