package exam2;

import java.util.Collection;
/**
 * Classify sound obtained from URLs by spectral densities
 * @author Anil Panda
 *
 */
public class ExamPart3 {

	public static void main(String[] args) {
		
		//urls of the instrument names 
		String instURL = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/index.txt";
		
		//urls of instrument recordings 
		String rec1 = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/recording01.txt";
		String rec2 = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/recording02.txt";
		String rec3 = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/recording03.txt";
		String rec4 = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/recording04.txt";
		
		//urls of software recordings 
		String genA = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/genA.txt";
		String genB = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/genB.txt";
		String genC = "http://www.hep.ucl.ac.uk/undergrad/3459/exam-data/2016-17/genC.txt";
		
		String spaceSplitter = "\\s+"; // Regex for any number of whitespaces

		try{			
			System.out.println("PART 3");
			
			//creating Recording variables from data using functions designed
			Recording record1 = Input.importRecording(rec1, spaceSplitter);
			Recording record2 = Input.importRecording(rec2, spaceSplitter);
			Recording record3 = Input.importRecording(rec3, spaceSplitter);
			Recording record4 = Input.importRecording(rec4, spaceSplitter);
			
			Recording record5 = Input.importRecording(genA, spaceSplitter);
			Recording record6 = Input.importRecording(genB, spaceSplitter);
			Recording record7 = Input.importRecording(genC, spaceSplitter);
			
			//Obtaining the Collection of sample amplitudes
			Collection<Integer> sample_rec1 = Input.importSample(rec1, spaceSplitter);
			Collection<Integer> sample_rec2 = Input.importSample(rec2, spaceSplitter);
			Collection<Integer> sample_rec3 = Input.importSample(rec3, spaceSplitter);
			Collection<Integer> sample_rec4 = Input.importSample(rec4, spaceSplitter);
			
			Collection<Integer> sample_rec5 = Input.importSample(genA, spaceSplitter);
			Collection<Integer> sample_rec6 = Input.importSample(genB, spaceSplitter);
			Collection<Integer> sample_rec7 = Input.importSample(genC, spaceSplitter);
			
			//Create objects, which method is used to classify sounds
			ClassifyByDensity cd = new ClassifyByDensity();
			
			//Use method designed to classify sounds for all of the recordings obtained
			String density1 = cd.classifySound(record1, sample_rec1);
			System.out.println("Density classification of recording 1: " + density1);
			System.out.println();
			

			String density2 = cd.classifySound(record2, sample_rec2);
			System.out.println("Density classification of recording 2: " + density2);
			System.out.println();
			
			String density3 = cd.classifySound(record3, sample_rec3);
			System.out.println("Density classification of recording 3: " + density3);
			System.out.println();
			
			String density4 = cd.classifySound(record4, sample_rec4);
			System.out.println("Density classification of recording 4: " + density4);
			System.out.println();
			
			String density5 = cd.classifySound(record5, sample_rec5);
			System.out.println("Density classification of genA: " + density5);
			System.out.println();
			
			String density6 = cd.classifySound(record6, sample_rec6);
			System.out.println("Density classification of genB: " + density6);
			System.out.println();
			
			String density7 = cd.classifySound(record7, sample_rec7);
			System.out.println("Density classification of genA: " + density7);
			System.out.println();


}
		//catch exceptions
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();

}
	}
	

}
