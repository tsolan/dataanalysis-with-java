package exam2;

/**
 * Method for creating Instrument object
 * @author Anil Panda
 *
 */
public class Instrument {

	//variables needed
	final String filename;
	final String instrument;
	
	/**
	 * Constructor
	 * @param filename
	 * @param instrument
	 */
	public Instrument(String filename, String instrument){
		this.filename = filename;
		this.instrument = instrument;
	}
	
	/**
	 * method for nice representation
	 */
	@Override
	public String toString() {
		return "Instrument [filename=" + filename + ", instrument=" + instrument + "]";
	}
}

