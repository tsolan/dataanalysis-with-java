package module6;

class LabelledDataPoint extends DataPoint{
	
	//creating additional variable 
	//using private to prevent direct access
	private String label;
	
	//Constructor for the class
	public LabelledDataPoint(double x, double y, double ey, String label) {
		
		//super uses the variables from superclass
		super(x, y, ey);
		
		this.label=label;
	}
	
	//Overriding toString() method to include data_point_label to representation
	@Override
	public String toString(){
		return label+": x = " + x + ", y = " + y + " +- " + ey;
	}
}
