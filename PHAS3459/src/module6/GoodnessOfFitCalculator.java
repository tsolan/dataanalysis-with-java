package module6;

import java.util.Collection;

//Calculate goodness of the fit of the data against theory
public interface GoodnessOfFitCalculator {
	
	//initialising a method used for calculation
	double goodnessOfFit(Collection<DataPoint> data, Theory theory); 
}
