package module6;

public class QuadraticTheory implements Theory {
	
	//creating needed variables
	protected double a, b, c;
	
	//creating a simple constructor
	public QuadraticTheory(double a, double b, double c){
		this.a = a;
		this.b = b;
		this.c = c;
	}

	//using interface method to define quadratic function
	public double y(double x) {
		double y = a*Math.pow(x, 2) + b*x + c;
		return y;
	}
	
	//adding method to represent the function
	public String toString(){
		return a + "x^2 + "+ b+ "x + " + c;
	}

}
