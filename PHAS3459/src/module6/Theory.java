package module6;

public interface Theory {
	
	//Defining methods without implementing them
	public double y(double x);
	
	public String toString();	

}
