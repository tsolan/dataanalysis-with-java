package module6;

import java.util.Collection;

import module6.DataPoint;
import module6.Theory;

public class ChiSquared implements GoodnessOfFitCalculator {

	//Method calculating chi-squared using theory method and given data
	public double goodnessOfFit(Collection<DataPoint> data, Theory theory) {
		
		//initialise variable
		double chiSquared = 0;
		
		//iterate over DataPoints
		for (DataPoint points : data) {
			
			//theoretical y
			double theoryY = theory.y(points.getX());
			
			//experimental y
			double expY = points.getY();
			
			//obtain top and bottom terms of chi-squared fraction
			double dY = Math.pow((expY-theoryY),2);
			double eySq = 1/(Math.pow(points.getEY(),2));
			
			//obtain chi-squared
			chiSquared += dY/eySq;
		}
			
		return chiSquared;

	}
}
