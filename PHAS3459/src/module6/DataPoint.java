package module6;

import java.util.Scanner;

public class DataPoint {
	
	//use protected, so it can be accessed by LabelledDataPoint
	//which extends DataPoint
	protected double x;
	protected double y;
	protected double ey;
	
	//simple constructor for the class
	public DataPoint(double x, double y, double ey){
		this.x = x;
		this.y = y;
		this.ey = ey;
	}
	
	//retrieve x-variable
	public double getX() {
		return x;
	}
	
	//retrieve y-variable
	public double getY() {
		return y;
	}
	
	//retrieve ey-variable
	public double getEY() {
		return ey;
	}
	
	//method for appropriately formatted data representation
	public String toString(){
		return "x = " + x + ", y = " + y + " +- " + ey;
	}

	public static DataPoint fromText(String line) throws Exception {

		// Instantiate Scanner from input String
		Scanner s = new Scanner(line);

		// Read DataPoint data from line
		// Read out 3 double data values
		double x = s.nextDouble();
		double y = s.nextDouble();
		double ey = s.nextDouble();

		// Close Scanner
		s.close();
		return new DataPoint(x, y, ey);

	}



}
	
