package module6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class DataAnalysis {

	public static void main(String[] args) {
		
		//String url to import data from
		String url = "http://www.hep.ucl.ac.uk/undergrad/3459/data/module6/module6-data.txt";
		
		try {
			//creating a collection of DataPoints from given url
			Collection<DataPoint> data = TestDataPoints.dataFromURL(url);
			
			//creating Theory function as given by exercise
			Theory func1 = new PowerLawTheory(2);
			Theory func2 = new PowerLawTheory(2.05);
			Theory func3 = new QuadraticTheory(1, 10, 0);
			
			//creating a Collection of theories to implement it in bestTheory method
			Collection<Theory> theories = new ArrayList<Theory>(Arrays.asList(func1,func2,func3));
			
			//goodness of fit using chi-squared
			GoodnessOfFitCalculator gofCalculator = new ChiSquared();
			
			//obtaining best theory from the data
			Theory bestTheory = bestTheory(data, theories, gofCalculator);
			System.out.println("The best theory is: "+bestTheory);
			
		} 
		
		//catching any exceptions
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}
	
	//copying a class given by exercise
    private static Theory bestTheory(Collection<DataPoint> data,
            Collection<Theory> theories, GoodnessOfFitCalculator gofCalculator) {
        boolean first = true;
        double bestGoodnessOfFit = 0.0;
        Theory bestTheory = null;
        for (Theory theory : theories) {
            double gof = gofCalculator.goodnessOfFit(data, theory);
            if (first) {
                bestTheory = theory;
                bestGoodnessOfFit = gof;
                first = false;
            } else if (gof < bestGoodnessOfFit) {
                bestTheory = theory;
                bestGoodnessOfFit = gof;
            }
        }
        return bestTheory;
    }

}
