package module6;

public class PowerLawTheory implements Theory {

	//creating a protected variable to represent the power
	protected double n;
	
	//adding a simple constructor
	public PowerLawTheory(double n){
		this.n = n;
	}

	//using interface method to define a function
	public double y(double x) {
		double y = Math.pow(x, n);
		return y;
	}
	
	//adding method to represent the function
	public String toString(){
		return "x^"+n;
	}
	

}
