package module6;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.URL;

import java.util.ArrayList;

import module6.DataPoint;

public class TestDataPoints {

	public static void main(String[] args) {
		
		// Initialise url String variable
		String url = "http://www.hep.ucl.ac.uk/undergrad/3459/data/module6/module6-data.txt";
		
		try {
			//Create an array of data points
			ArrayList<DataPoint> urldata = dataFromURL(url);
			
			System.out.println(urldata.size());
			//loop over elements of the ArrayList
			for (DataPoint element: urldata){
				System.out.println(element);
			}			
		} 
		
		//catch any exceptions
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	//Method acquiring data from url and put it in ArrayList of DataPoints 
	public static ArrayList<DataPoint> dataFromURL(String url) throws Exception{
		
		//Initialise variables
		String line = "";
		ArrayList<DataPoint> urlData = new ArrayList<DataPoint>();
		
		// Open URL from which to read data
		URL u = new URL(url);
		InputStream is = u.openStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		
		while ((line = br.readLine()) != null) {

			//Split the line using whitespace regex and add the elements to the array
			String data[] = line.split("\\s+");
			
			//if there is 3 elements in array, add a DataPoint to ArrayList
			if (data.length == 3) {
				
				//Array that will contain doubles instead of Strings in data[]
				double values[] = new double[3];
				
				//Convert String to Double and add them to values[]
				for (int i = 0; i<data.length; i++){
					values[i] = Double.parseDouble(data[i]);
				}
				
				//Finally, create a DataPoint object with the data obtained and add it to ArrayList
				urlData.add(new DataPoint(values[0], values[1], values[2]));
			}
			
			
			//if there is 3 elements in array, add a LabelledDataPoint to ArrayList
			else if (data.length == 4) {
				
				//Array that will contain doubles instead of Strings in data[]
				double values[] = new double[3];
				
				//Convert String to Double and add them to values[]
				for (int i=0; i<values.length-1; i++) {
					values[i] = Double.parseDouble(data[i]);
				}
				
				//Finally, create a LabelledDataPoint object with the data obtained and add it to ArrayList
				urlData.add(new LabelledDataPoint(values[0], values[1], values[2], data[3]));
			}
			
			//if there is a different number of elements in the line, throw an exception
			else {
				throw new IllegalArgumentException("There is other than 3 or 4 values in the line.");
			}

		}

		// Close BufferedReader
		br.close();

		// Return ArrayList containing all the relevant data
		// System.out.println(urlData.size());
		return urlData;


	}

}