package module2;

public class ParticleMain {
	
    public static void main(String[] args) {
        double deltaT[] = {0.5, 0.1, 0.01, 0.001, 0.0001}; //array of small time intervals used
        FallingParticle fallingParticle = new FallingParticle(6.3, 4.1); //creating a new particle using class previously described

        for (double aDeltaT : deltaT) { //using for cycle to go through all elements of an array

            fallingParticle.setZ(8); //setting vessel height as 8
            fallingParticle.drop(aDeltaT); //simulating fall of the function using previously described function
            System.out.println("deltaT = " + aDeltaT + "\nv = " + fallingParticle.getV() + " t = " + fallingParticle.getT()); //printing result

        }
        System.out.println("As it can be seen, the values obtained are different from each other. This is happening because doTimeStep() function");
        System.out.println("is then takes different time intervals to update v and t. This means for lower deltaT it will produce more loops; therefore,");
        System.out.println("more counting of velocity and height and getting values of v and t at the point much closer to zero height.");
    }
}



