package module2;

public class ThreeVector {
	
	private double x,y,z;
	//Here x,y,z are coordinates of a vector
	
	//Adding needed constructor, so we can use our ThreeVector with or without coordinates.
	public ThreeVector(){}
	public ThreeVector(double a, double b, double c){
		x = a;
		y = b;
		z = c;	
	}
	//Simply getting magnitude from the ThreeVector object
	public  static double magnitude (ThreeVector vec1){
		double mag = Math.sqrt(vec1.x*vec1.x + vec1.y*vec1.y + vec1.z*vec1.z); //using square root function of Math module to obtained the magnitude from squared value
		return mag;
	}	
	//Getting unit vector from the ThreeVector object 
	public static ThreeVector unitVector (ThreeVector vec1){
		double mag = magnitude(vec1); //using another function to obtain magnitude
		double new_x = vec1.x/mag; //obtaining x,y,z coordinates of a unit vector
		double new_y = vec1.y/mag;
		double new_z = vec1.z/mag;
		ThreeVector vec = new ThreeVector(new_x,new_y,new_z);	//putting obtained coordinates in our vector object
		return vec;
	}
	//Returning a string from the ThreeVector object
	public static String toString(ThreeVector vec1){
		String string_x = String.valueOf(vec1.x); //string values of each coordinate
		String string_y = String.valueOf(vec1.y);
		String string_z = String.valueOf(vec1.z);
		String string_v = "("+string_x +", "+ string_y +", "+ string_z+")"; //putting everything together in nice way
		return string_v;
	}
	//Returning a scalar product of 2 vectors
	public static double scalarProduct(ThreeVector vec1, ThreeVector vec2){
		double dotprod = vec1.x*vec2.x + vec1.y*vec2.y + vec1.z*vec2.z; //just multiplying needed values as usual
		return dotprod; 
	}
	//Returning an angle between 2 vectors
	public static double angle(ThreeVector vec1, ThreeVector vec2){
		double dotp = scalarProduct(vec1, vec2); //obtain a dot product of these vectors
		double vector1_mag = magnitude(vec1); //obtain magnitude of 1st vector
		double vector2_mag = magnitude(vec2); //obtain magnitude of 2nd vector
		
		double cosine = dotp/(vector1_mag*vector2_mag);  //getting cosine by division, since a*b = |a||b|cos(alpha) by definition
		double angle = Math.toDegrees(Math.acos(cosine)); //finding arccosine using Math function and 
		//converting the value from radians to degrees for convinience.
		
		return angle;
	}
	//Returning vector obtained from adding 2 vectors
	public static ThreeVector add(ThreeVector vec1, ThreeVector vec2){
		ThreeVector vec3 = new ThreeVector(); 
		vec3.x = vec1.x + vec2.x; //adding up each coordinate and putting them into the new vector object
		vec3.y = vec1.y + vec2.y;
		vec3.z = vec1.z + vec2.z;
		return vec3;
	}
	//Returning vector product of 2 vectors
	public static ThreeVector vectorProduct(ThreeVector vec1, ThreeVector vec2){
		ThreeVector vec3 = new ThreeVector();
		vec3.x = vec1.y*vec2.z-vec1.z*vec2.y; //obtaining coordinates of vector product using formula and
		vec3.y = vec1.y + vec2.y;             //putting them into the new vector object
		vec3.y = vec1.z*vec2.x-vec1.x*vec2.z;
		vec3.z = vec1.x*vec2.y-vec1.y*vec2.x;
		return vec3;
	}
	//The following code just uses static method to represent them in instance way
	public double scalarProd(ThreeVector vec1, ThreeVector vec2){
		return scalarProduct(vec1, vec2);
	}
	
	public double ang(ThreeVector vec1, ThreeVector vec2){
		return angle(vec1, vec2);
	}
	
	public ThreeVector adding(ThreeVector vec1, ThreeVector vec2){
		return add(vec1, vec2);
	}
	
	public ThreeVector vectorProd(ThreeVector vec1, ThreeVector vec2){
		return vectorProduct(vec1,vec2);
	}
	
		
		
	

}
