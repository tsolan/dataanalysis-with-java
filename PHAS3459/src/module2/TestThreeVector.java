package module2;

public class TestThreeVector {

	public static void main(String[] args) {
		
		//Adding our vector objects
		ThreeVector vec1 = new ThreeVector(5,2,3);
		ThreeVector vec2 = new ThreeVector(4,5,1);
		ThreeVector vec3 = new ThreeVector(0,0,0);
		
		//UNIT VECTOR + TOSTRING TEST
		System.out.println("Unit vector of vec1: "+ThreeVector.toString(ThreeVector.unitVector(vec1)));
		System.out.println("Unit vector of vec2: "+ThreeVector.toString(ThreeVector.unitVector(vec2)));
		System.out.println("Unit vector of vec3: "+ThreeVector.toString(ThreeVector.unitVector(vec3)));
		
		//SCALAR PRODUCT TEST
		System.out.println("Scalar product of vec1 and vec2: "+ ThreeVector.scalarProduct(vec1,vec2));
		System.out.println("Scalar product of vec1 and vec3: "+ ThreeVector.scalarProduct(vec1,vec3));
		
		System.out.println("Scalar product of vec1 and vec2(non-static): "+ vec1.scalarProd(vec1,vec2));
		System.out.println("Scalar product of vec1 and vec3(non-static): "+ vec1.scalarProd(vec1,vec3));
		
		//VECTOR PRODUCT TEST
		System.out.println("Vector product of vec1 and vec2: "+ ThreeVector.toString(ThreeVector.vectorProduct(vec1,vec2)));
		System.out.println("Vector product of vec1 and vec3: "+ ThreeVector.toString(ThreeVector.vectorProduct(vec1,vec3)));
		
		System.out.println("Vector product of vec1 and vec2(non-static): "+ ThreeVector.toString(vec1.vectorProd(vec1,vec2)));
		System.out.println("Vector product of vec1 and vec2(non-static): "+ ThreeVector.toString(vec1.vectorProd(vec1,vec3)));
		
		System.out.println(vec1);
		System.out.println("As it can be seen, the output is not resolvable. This is the way java stores information about our object");
		


	}

}