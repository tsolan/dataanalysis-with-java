package module2;

public class TestComplex {

	public static void main(String[] args) {
		Complex c1 = new Complex(2,-1);  
		Complex c2 = new Complex(-1,2);  //creating needed complex numbers
		Complex i = new Complex(0,1); //i in our complex system is defined like 0+1*i
		Complex zero = new Complex(0,0); //define 0
		
		  
		// Perform operations  
		System.out.println("Product of c1 and c2: "+ Complex.toString(Complex.multiply(c1, c2)));  
		System.out.println("Ratio of c1 and c2: "+ Complex.toString(Complex.divide(c1, c2)));  
		System.out.println("Product of c1 and i: "+ Complex.toString(Complex.multiply(c1, i)));  
		System.out.println("Ratio of c1 and 0: "+ Complex.toString(Complex.divide(c1, zero)));  //returns null since it is impossible
     	System.out.println("Product of c1 and c2*: "+ Complex.toString(Complex.multiply(c1, Complex.conjugate(c2))));  
		System.out.println("Product of c2 and c2*: "+ Complex.toString(Complex.multiply(c2, Complex.conjugate(c2))));  
		System.out.println("As it can be seen all values obtained are correct");

	}

}
