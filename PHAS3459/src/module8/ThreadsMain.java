package module8;


/**
 * 
 * @author Anil Panda
 *
 */
public class ThreadsMain {

	public static void main(String[] args) {
		
		//creating separate threads for our tasks
		Thread countdown = new Thread(new CountdownTask(10)); //19 seconds countdown
		Thread prime  =  new Thread(new PrimeNumberTask());
		
		//starting both tasks at the same time
		countdown.start();
		prime.start();
		
		
		//waiting for countdown to finish
		try {
			countdown.join();
		}
		
		//catch exceptions
		catch (InterruptedException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		//after countdown is finished, interrupt prime number calculation
		prime.interrupt();
	}

}
