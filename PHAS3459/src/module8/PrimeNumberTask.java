package module8;

import java.util.ArrayList;

/**
 * Class for finding prime numbers 
 * @author Anil Panda
 *
 */
public class PrimeNumberTask implements Runnable{
	
	//ArrayList for prime numbers
	private ArrayList<Integer> list = new ArrayList<Integer>();
	
	//getter function for prime numbers
	public ArrayList<Integer> getPrimes() {
		return list;
	}

	
	public void run() {	
		int n = 2;
		
		while (true) {
			if (prime(n) == true){
				list.add(n);
			}
			
			//go to the next integer
			n++;
			
			//if the thread is interrupted print required values and return the function
			if (Thread.currentThread().isInterrupted()){
				
				System.out.println("Largest integer checked: " + n);
				System.out.println("Largest prime found: " +  list.get(list.size() - 1));
				System.out.println("Total number of primes: " + list.size());
				return;
			}
		}
	}
	
	/**
	 * 
	 * @param n - integer number checked to be prime or not
	 * @return boolean true if prime, false if not
	 */
	public boolean prime(int n){
		
		//starting from 2 to n-1 since everything is divisible by 1 and itself
		for (int i=2;i<n;i++){
			//if there is any number in this range divides n, it is not prime
			if (n % i == 0){
				return false;
			}
		}	
		//return true if nothing divides n
		return true;
		
	}

}
