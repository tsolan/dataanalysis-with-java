package module8;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 
 * @author Anil Panda
 *
 */
public class ThreadsTimer {

	public static void main(String[] args) {
		
		//using both methods to obtain pi
		OneThreadTime();
		System.out.println();
		FourThreadTime();
		
		System.out.println();
		System.out.println("As it can be seen, time taken using 4 threads is roughly 4 times smaller");
		System.out.println("than the time for one thread. This is predictable, since using 4 threads means doing");
		System.out.println("4 processes simultaniously, which results in efficient time reduction. However,");
		System.out.println("this splitting does not give a better value of pi, both of the values has some error to");
		System.out.println("the actual value");
	}
	
	//Method calculation pi value using Monte Carlo method in one thread
	public static void OneThreadTime(){
		
		
		long start = System.currentTimeMillis(); //start of calculation time
		long nPoints = 10000000L; //number of random points
		
		MonteCarloPiCalculatorTask task = new MonteCarloPiCalculatorTask(nPoints); //using method for given points
		
		double pi = task.call();
		System.out.println("One thread calculation pi value:" + pi); //obtain pi value
		
		long stop = System.currentTimeMillis(); //end of calculation time
		long time = stop - start; //time for calculation in ms
		System.out.println("One thread time taken to calculate pi: " + time + "ms");
	}
	
	//Method calculation pi value using Monte Carlo method in four threads
	public static void FourThreadTime(){
		
		long start = System.currentTimeMillis(); //start of calculation time
		long nPoints = 10000000L; //number of random points
		int nThreads = 4; //number of threads the task uses
		
		ExecutorService threadPool = Executors.newFixedThreadPool(nThreads); //create a thread pool for our task
		List<Future<Double>> futures = new ArrayList<Future<Double>>();
		
		//separates the task between threads
		for (int iThread = 0; iThread < nThreads; ++iThread) {
			MonteCarloPiCalculatorTask task2 = new MonteCarloPiCalculatorTask(nPoints/nThreads);
			Future<Double> future = threadPool.submit(task2);
			futures.add(future);
		}
		
		//retrieve result from all threads to obtain the final result
		double sum = 0.0;
		for (int iThread = 0; iThread < nThreads; ++iThread) {
			
			//initialise the variable
			double result;
			
			//get result from futures ArrayList from every thread and sum them up
			try {
				result = futures.get(iThread).get();
				sum += result;
			} 
			
			//catch exceptions
			catch (InterruptedException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			} 
			catch (ExecutionException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		
		threadPool.shutdown(); //shutdown the pool
		double pi2 = sum/nThreads; //obtain pi
		System.out.println("Four threads calculated pi value: " + pi2);
		
		long stop = System.currentTimeMillis();	
		long time = stop - start;
		System.out.println("Four threads time taken to calculate pi: " + time + "ms");
	}

}
