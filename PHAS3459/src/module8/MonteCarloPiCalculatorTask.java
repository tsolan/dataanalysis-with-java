package module8;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * 
 * @author Anil Panda
 *
 */
public class MonteCarloPiCalculatorTask implements Callable<Double> {
	private final long n_points;
	
	//simple constructor for the class
	public MonteCarloPiCalculatorTask(long nPoints) {
		this.n_points = nPoints;
	}
	
	//calculation of pi using Monte Carlo method
	public Double call() {
		Random rand = new Random();
		long n_in = 0;
		for (long iPoint = 0; iPoint < n_points; ++iPoint) {
			double x = rand.nextDouble();
			double y = rand.nextDouble();
			double r2 = x*x + y*y;
			if (r2 < 1.0) ++n_in;
		}
		return 4.0 * n_in / n_points; //value of pi
	}
}
