package module8;
/**
 * Class doing countdown by seconds
 * @author Anil Panda
 *
 */
public class CountdownTask implements Runnable {
	
	//number of seconds to countdown from
	int secs;
	
	//simple constructor for the class
	public CountdownTask (int secs) {
		this.secs = secs;
	}

	public void run() {
		
		//obtaining milliseconds from given seconds value
		long millis = secs*1000;
		
		//represents the time in milliseconds, when the loop starts
		long start = System.currentTimeMillis();
		
		//needed because the loop does more than one iteration over the time of 1 second
		boolean printed = false;
		
		//runs until the time from the start is the same as countdown time
		while (System.currentTimeMillis()-start <= millis){
			
			//if the time from the start is divisible by 1000 means that integer number of seconds passed
			//so it will be printed if it haven't yet been printed
			if ((System.currentTimeMillis()-start) % 1000 == 0 && printed == false){
				printed = true;
				System.out.println((millis-(System.currentTimeMillis()-start))/1000);
				
				//print done when countdown reaches 0
				if ((millis-(System.currentTimeMillis()-start)) == 0){
					System.out.println("Done");
				}
			}
			
			//"refresh" printed variable after each integer number of seconds
			if ((System.currentTimeMillis()-start) % 1000 != 0){
				printed = false;
			}
			

		}	
	}

}
