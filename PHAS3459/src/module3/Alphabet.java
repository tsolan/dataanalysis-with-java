package module3;
import java.util.Random;

public class Alphabet {

	public static void main(String[] args) {
		StringBuilder s = new StringBuilder(); //Creating string, to which all characters will be appended
		int number_of_ex = 0; //variable for number of exceptions
		int number_sum = 0; //variable for the sum value
		
		for(int i=0;i<=400;i++){  //loop creating 400 random characters and checking them
		  char ch = randomCharacter();
		  
		  if (Character.isDigit(ch) || Character.isLetter(ch)){ //runs only if created variable is letter or number
			  try {
				  number_sum+=Integer.parseInt(Character.toString(ch)); //tries to convert value to integer and then add it to the sum
			  }
			  catch (Exception e){
				  number_of_ex+=1;
			  }
			  s.append(ch); //appending our random value to string
		  }
		}
	    System.out.println("The string is: "+s.toString()); 
	    System.out.println("The sum of all numbers  is: "+ number_sum); 
        System.out.println("The number of exceptions is :" + number_of_ex); 
        

		
	}
	
	//random character 0-127 generator
	public static char randomCharacter(){ //Creating random character generator
		Random r = new Random(); //using imported random function
		int r_int = r.nextInt(127); //will use 128 symbols
		char r_char = (char) r_int; //convert integer random number to char
		return r_char;
	}

}
