package module3;

public class TestExceptions {

	public static void main(String[] args) throws Exception {
		
		//Testing exceptions of Complex class methods
		System.out.println("Exceptions of Complex class:");
		Complex test_complex = new Complex(1.0,1.0);
		Complex zero = new Complex(0.0,0.0); //zero complex number needed for our exceptions
		try{
		    Complex.divide(test_complex,zero);//checking zero division 
		}
		catch (Exception e){
			System.out.println(e); //printing out our error message
		}
		try{
		    Complex.normalised(zero); //trying to normalise zero complex number
		}
		catch (Exception e){
			System.out.println(e); //printing out our error message
		}
		System.out.println();
		
		//Testing exceptions of ThreeVector class methods
		System.out.println("Exceptions of ThreeVector class:");
		ThreeVector test_vec = new ThreeVector(1.0,1.0,1.0);
		ThreeVector zero_vec = new ThreeVector(0.0,0.0,0.0); //zero vector is needed for our exceptions
		try{
			ThreeVector.unitVector(zero_vec); //trying to calculate unit vector for zero-vector
		}
		catch (Exception e){
			System.out.println(e); //printing out our error message
		}
		try{
			ThreeVector.angle(test_vec, zero_vec); //trying to obtain angle between vector and zero-vector
		}
		catch (Exception e){
			System.out.println(e); //printing out our error message
		}
		System.out.println();
		
		//Testing exceptions of FallingParticle class methods
		System.out.println("Exceptions of FallingParticle class:");
		FallingParticle test_particle = new FallingParticle(1.0,1.0);
		try{
			FallingParticle neg_mass = new FallingParticle(-1.0, 4.0); //trying to create an object with negative mass
		}
		catch (Exception e){
			System.out.println(e); //printing out our error message
		}
		try{
			
			test_particle.setZ(-1.0); //trying to set height to be negative
		}
		catch (Exception e){
			System.out.println(e); //printing out our error message
		}

	}

}
