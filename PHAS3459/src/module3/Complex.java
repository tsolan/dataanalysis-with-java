package module3;

public class Complex {
	
	private double x,y; //x - real part, y - imaginary
	//Constructor
	public Complex(){};
	public Complex(double a, double b){ //needed constructors
		x = a;
		y = b;
	}
	
	public static double real(Complex c){ //returns real part
		return c.x;
	}
	
	public static double imag(Complex c){ //returns imaginary part
		return c.y;
	}
	
	public static double modulus(Complex c){ //returns modulus of complex number
		double mod = Math.sqrt(c.x*c.x + c.y*c.y); //basic modulus formula
		return mod;
	}
	
	public static Complex normalised(Complex c) throws Exception{
		double mod = modulus(c); //obtain the modulus
		if (mod==0){
			throw new Exception("Complex number is zero, impossible to normalise.");
		}
		Complex normalised_c = new Complex(); //create a new complex value that will store normalised c
		normalised_c.x = c.x/mod; //getting real and imaginary parts for normalised value
		normalised_c.y = c.y/mod;
		return normalised_c;
	}
	
	public static double angle(Complex c){
		double ang;
		if (c.x>=0 && c.y>=0 || c.x<0 && c.y<0)
			{ang = Math.toDegrees(Math.atan(c.y/c.x)); } //1 and 4 quadrant
		if (c.x<0 && c.y>0)
		    {ang = Math.toDegrees(Math.atan(c.y/c.x)+Math.PI); } //2nd quadrant
		else
	        {ang = Math.toDegrees(Math.atan(c.y/c.x)-Math.PI); } //3rd quadrant
		return ang;

	}
	
	public static Complex conjugate(Complex c){ //returns complex conjugate
		Complex conj = new Complex(); //creating a new complex number
		conj.x = c.x;
		conj.y = -c.y; //this gives conjugate
		return conj;
	}
	public boolean equals(Complex c){ //returns if the complex numbers are equal or not in boolean
		boolean equality = false;
		if (this.x == c.x && this.y == c.y) //check of equality
			equality = true; //if they are equal, change value to True
		return equality;
		
	}
	public static String toString(Complex c){
		String string_x = String.valueOf(c.x);
		String string_y = String.valueOf(c.y);
		String string_value = null;
		if (c.y>0){
		    string_value = (string_x+"+"+string_y+"i"); //display complex number in form of a+bi if y>0
		    }
		if (c.y==0){
			string_value = (string_x); //does not display iy if y is zero
		}
		if (c.x==0){
			string_value = (string_y); //does not display x if it is zero
		if (c.x==0 && c.y==0){
			string_value = "0";
		}
		}
		if (c.y<0){
			string_value = (string_x+string_y+"i"); //if y<0, plus sign is not needed
		}
		return string_value;
}
	public static Complex add(Complex c, Complex b){ //addition formula for complex numbers
		Complex a = new Complex();
		a.x = c.x + b.x;
		a.y = c.y + b.y;
		return a;
	}
	public static Complex subtract(Complex c, Complex b){ //subtraction formula for complex numbers
		Complex a = new Complex();
		a.x = c.x - b.x;
		a.y = c.y - b.y;
		return a;
	}
	public static Complex multiply(Complex c, Complex b){ //multiplication formula for complex numbers
		Complex a = new Complex();
		a.x = c.x*b.x - c.y*b.y;
		a.y = c.y*b.x + b.y*c.x;
		return a;
	}
	public static Complex divide(Complex c, Complex b) throws Exception{ //division formula for complex numbers
		if (b.x==0 && b.y==0){
			throw new Exception("There is a division by a zero complex number.");
		}
		Complex a = new Complex();
		a.x = (c.x*b.x + c.y*b.y)/(b.x*b.x+b.y*b.y);
		a.y = (c.y*b.x-c.y*b.y)/(b.x*b.x+b.y*b.y);
		return a;
	}

}
