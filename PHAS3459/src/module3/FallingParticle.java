package module3;

public class FallingParticle {
	private double m; //mass
    private double d; //drag coefficient
    private double t; //time
    private double v; //velocity
    private double z; //height
    private final static double g = 9.8; //g is set to be final since it is not changing

    public FallingParticle(double m, double d) throws Exception { //creating a constructor
        this.m = m;
        if (this.m<0){
        	throw new Exception("Mass value is not positive.");
        }
        this.d = d;
    }
    
    //adding needed setters and getters

    public double getZ() { //getting the value for the height
        return z;
    }

    public void setZ(double z) throws Exception { //setting the value for the height
        this.z = z;
        if (this.z<0){
        	throw new Exception("The height value is not positive.");
        }
    }

    public double getV() { //getting the value for the velocity
        return v;
    }

    public void setV(double v) { //setting the value for the velocity
        this.v = v;
    }

    public double getT() { //getting the value for time
        return t;
    }
    
    //time step function
    void doTimeStep(double deltaT){
        double a;
        a = (d * v * v / m) - g;
        v += a * deltaT;         
        z += v * deltaT; //updates position, velocity according to deltaT
    }

    //This function simulates the falling particle
    void drop(double deltaT){
        v=0;
        t=0; //initial parameters for velocity and time
        while (z>=0) {
            doTimeStep(deltaT);
            t+=deltaT;
        } //runs until height = 0 (which is when it hits the ground

    }


}
