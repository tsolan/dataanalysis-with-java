package module4;

import java.io.*;

import java.net.*;
import java.util.Scanner;



public class WordCounter {
	public static void main(String[] args){
			BufferedReader check;
			try {
				check = brFromURL("http://www.hep.ucl.ac.uk/undergrad/3459/data/module4/module4_text.txt");
				//provided url, the word count is checked to be 456 using MS Word
				int count = countWordsInResource(check);
				System.out.println(count);
				// Catch exception for invalid URL 	
			} 
			catch (MalformedURLException m) { 
				System.out.println("Invalid URL: "+m.getMessage()); 
		 		//Catch exception for invalid input
			} 
			catch (IOException i) { 
				System.out.println("Invalid input: "+i.getMessage()); 
				// Catch everything else
			} 
			catch (Exception e) { 
				System.out.println("Error: "+e.getMessage()); 
			} 



	}
	
	//method taking url name and creating a BufferedReader object of web-site 
	public static BufferedReader brFromURL(String urlName) throws IOException{
		URL u = new URL(urlName); //creating a url object, which has the name specified
		InputStream is = u.openStream(); //
		InputStreamReader isr = new InputStreamReader(is); //read from InputStream
		BufferedReader b = new BufferedReader(isr); //Make a BufferedReader
	    return b;
	}
	
	//method taking file name a creating a BufferedReader object of the file
	public static BufferedReader brFromFile(String fileName) throws FileNotFoundException{
		FileReader fr = new FileReader(fileName);
		BufferedReader br = new BufferedReader(fr);
		return br;
	}
	
	public static int countWordsInResource(BufferedReader dataAsBR){
		Scanner s = new Scanner(dataAsBR); //Scanner is used to work with acquired data
		int number_of_words = 0; //initial word count
		while(s.hasNext()){ //if there is another word coming, add 1 to word count

				number_of_words++;
			
		}
		s.close();
		return number_of_words;
	}

}
