package module4;

import java.io.*;
import java.net.*;

public class NumericalReader {

	public static void main(String[] args) {
		
		String url = "http://www.hep.ucl.ac.uk/undergrad/3459/data/module4/module4_data1.txt"; //our URL
		NumericalReader nr = new NumericalReader();
		
		try {
			BufferedReader br = brFromURL(url);	
			String line = ""; //line string, used in methods
			nr.analysisStart(); //creates file, starts analysis
				
			while ((line = br.readLine()) != null) { 
				try {
					nr.analyseData(line); //analysis each line
				}
				catch (IndexOutOfBoundsException iobe) {iobe.printStackTrace(); continue;} 
			}
			nr.analysisEnd();
		}
		// Catch exception for invalid URL 	
		catch (MalformedURLException m) { 
			System.out.println("Invalid URL: "+m.getMessage()); 
		}
		catch (IOException i) { 
			System.out.println("Invalid input: "+i.getMessage()); 
			// Catch everything else
		} 
		catch (Exception e) { 
			System.out.println("Error: "+e.getMessage()); 
		} 
		

		


	}
	
	public static String getStringFromKeyboard() throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); //reader from keyboard
		String str = reader.readLine(); //string from the reader
		return str;
	}
	
	public static BufferedReader brFromURL(String urlName) throws IOException{
		
		URL u = new URL(urlName); //creating a url object, which has the name specified
		InputStream is;
		is = u.openStream();
		InputStreamReader isr = new InputStreamReader(is); //read from InputStream
		BufferedReader br = new BufferedReader(isr); //Make a BufferedReader
	
	    return br;
	}
	
	private double minValue, maxValue, nValues, sumOfValues;
	private PrintWriter printwriter;
	
	void analysisStart() throws IOException{
		String file_directory = "N:"+File.separator+"numbers.txt";
		FileWriter fw = new FileWriter(file_directory);
		BufferedWriter writer = new BufferedWriter(fw);
		printwriter = new PrintWriter(writer);
		minValue = Double.MAX_VALUE;
		maxValue = Double.MIN_VALUE; //use this, so we guarantee that there will be smaller/larger number in a set
		nValues = 0.0;
		sumOfValues = 0.0;
	}
	
	
	void analyseData(String line){
		if (line.isEmpty() || Character.isLetter(line.charAt(0))) {}
		else {
			String[] nArr = line.split("\\s+"); //here I user regular expression, which contributes to any number of spaces
			
			for(int i=0;i<nArr.length;i++){ //convert elemets of array to double eache iteration to work with it
				double num = Double.parseDouble(nArr[i]);
				System.out.println(num);
				printwriter.println(num);
				minValue = (num < minValue ? num : minValue); //if number is lower then minimum, replace it with it
				maxValue = (num > maxValue ? num : maxValue); ////if number is lower then minimum, replace it with it
				nValues += 1; //add 1 to the count
				sumOfValues += num; //add number to the sum
			}
				
		}
		
	}
	
	void analysisEnd(){
		System.out.println("Maximum value: " + minValue);
		System.out.println("Minimum value: " + maxValue);
		System.out.println("Number of values:" + nValues);
		System.out.println("Sum of all values: " + sumOfValues);
	}

}
