package module5;

public class DataPoint {
	private double x, y, ey;
	
	public DataPoint(double cx, double cy, double cey){
		x=cx;
		y=cy;
		ey=cey;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getEY() {
		return ey;
	}
	
	public String toString(){
		return "("+x+","+y+","+ey+")";
	}

}
