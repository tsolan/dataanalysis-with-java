package module5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Scanner;

public class Minerals {

	public static void main(String[] args) throws IOException {
		Samples(loadLocations()); //using both functions
	}
	
	//obtaining locations using Scanner
	public static HashMap<Integer, String> loadLocations() throws IOException{
		HashMap<Integer, String> name = new HashMap<Integer, String>();
		URL locations = new URL("http://www.hep.ucl.ac.uk/undergrad/3459/data/module5/module5-locations.txt");	
		InputStream inputstream = locations.openStream();
		InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
		BufferedReader br = new BufferedReader(inputstreamreader);
		@SuppressWarnings("resource")
		Scanner s = new Scanner(br);
		while(s.hasNextLine()){
			String[] parts = s.nextLine().split("\\s+");
			name.put(Integer.parseInt(parts[1]),parts[0]);
		}
		return name;
	}
	
	//Obtaining maximum and minimum masses and getting their code number and location
	public static void Samples(HashMap<Integer, String> name) throws IOException{
		double max_mass = 0;
		double min_mass = 1000000;
		String max_location="";
		String min_location="";		
		int max_code = 0;
		int min_code =0;
		URL samples = new URL("http://www.hep.ucl.ac.uk/undergrad/3459/data/module5/module5-samples.txt");	
		InputStream inputstream = samples.openStream();
		InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
		BufferedReader br = new BufferedReader(inputstreamreader);
		@SuppressWarnings("resource")
		Scanner s = new Scanner(br);
		while(s.hasNextLine()){
			String[] parts = s.nextLine().split("\\s+");
			String n = (String) name.get(Integer.parseInt(parts[0]));
			int code = Integer.parseInt(parts[0]);
			double mass = Double.parseDouble(parts[1]);
			if(max_mass<mass){
				max_mass=mass; 
				max_location = n; 
				max_code=code;}
			
			if(min_mass>mass){
				min_mass=mass; 
				min_location =n; 
				min_code=code;}
		}
		System.out.println("The sample with highest mass has code: "+max_code+"; it has mass of: "+max_mass+
				" gramms; its location is: "+max_location);
		System.out.println("The sample with lowest mass has code "+min_code+"; it has mass of: "+min_mass+
				" gramms; its location is: "+min_location);
	}

}
