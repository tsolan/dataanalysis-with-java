package module5;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class DataAnalysis {

	public static void main(String[] args) {
		double x2 = goodnessOfFit(new Theory(2), dataFromURL("http://www.hep.ucl.ac.uk/undergrad/3459/data/module5/module5-xy.txt"));
		double x4 = goodnessOfFit(new Theory(4), dataFromURL("http://www.hep.ucl.ac.uk/undergrad/3459/data/module5/module5-xy.txt"));
		System.out.println("Chi squared value for y=x^2 is: " +x2);
		System.out.println("Chi squared value for y=x^4 is: " +x4);	
		if (x2<x4) {
			System.out.println("\n" + "y=x^2 is a better fit for the data.");}
		else {
			System.out.println("\n" + "y=x^4 is a better fit for the data.");
		}
	
	}
	

	//Method obtaining data to work with from given url
	static ArrayList<DataPoint> dataFromURL(String url){
		ArrayList<DataPoint> tokens = new ArrayList<DataPoint>();
		try{
			URL u = new URL(url);
			InputStream inputstream = u.openStream();
			InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
			BufferedReader br = new BufferedReader(inputstreamreader);
			@SuppressWarnings("resource")
			Scanner s = new Scanner(br);
			while(s.hasNextLine()){
				String[] parts = s.nextLine().split("\\s+");
				tokens.add(new DataPoint(
						Double.parseDouble(parts[0]),
						Double.parseDouble(parts[1]),
						Double.parseDouble(parts[2])));
			}
		}
		//catching exceptions
		catch(FileNotFoundException e){
			System.out.println("Error: File not found: "+e.getMessage());		
		}
		catch(IOException e){
			System.out.println("Error: "+e.getMessage());
		}
		return tokens;
	}
	
	//Method calculating chi squared using thery method and given data
	static double goodnessOfFit(Theory theory, ArrayList<DataPoint> data){
		double chisquared = 0.0;
		Iterator<DataPoint> it = data.iterator();
		while (it.hasNext()){
			DataPoint line = it.next();
			chisquared += Math.pow(line.getY()-theory.y(line.getX()),2)/Math.pow(line.getEY(), 2);
		}
		return chisquared;
	}
	

}
