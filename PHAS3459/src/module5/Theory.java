package module5;

public class Theory {
	private double n;
	public Theory(double cn){
		n=cn;
	}
	public double y(double x){
		double power_func = Math.pow(x, n);
		return power_func;
	}
}
