package module5;

import java.util.Arrays;

public class SquareMatrix {
	private double[][] matrix; //will use this as elements of matrix array

	//Method checking if our matrix is a square matrix
	public SquareMatrix(double[][] elements) throws Exception{
		for(int i=0; i<elements.length; i++){
			if (elements[i].length != elements.length) throw new Exception("Number of rows is not equal to number of columns!");
		}
		matrix=elements;
	}
	
	//Method for appropriate representation of a matrix
	public String toString(){
		String representation = "(";
		for (int i=0;i<matrix.length;i++){
			representation += "(";
			for (int j=0;j<matrix[i].length;j++){
				representation += matrix[i][j];
				if(j+1!=matrix[i].length){
					representation +=",";}
			}
			representation += ")";
			if(i+1!=matrix.length){
				representation +=",";}
		}
		representation += ")";
		return representation;
	}
	
	//Method returning unit matrix of given size
	public static SquareMatrix unitMatrix(int size) throws Exception{
		double[][] elements = new double[size][size];
		for (int i=0;i<size;i++){
			for (int j=0;j<size;j++){
				if (i==j){
					elements[i][j]=1;
				}
				else{
					elements[i][j]=0;
				}
			}
		}
		SquareMatrix unit_matrix = new SquareMatrix(elements);
		return unit_matrix;
	}

	//Method checking the equality of matrices(created by Eclipse)
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SquareMatrix other = (SquareMatrix) obj;
		if (!Arrays.deepEquals(matrix, other.matrix))
			return false;
		return true;
	}
	
	//Method used to add 2 matrices
	public static SquareMatrix add(SquareMatrix sm1, SquareMatrix sm2) throws Exception{
		double[][] elements = new double[sm1.matrix.length][sm1.matrix[0].length];
		SquareMatrix sum = new SquareMatrix(elements);
		for (int i=0;i<sm1.matrix.length;i++){
			for (int j=0;j<sm1.matrix[0].length;j++){
				elements[i][j] = sm1.matrix[i][j] + sm2.matrix[i][j];
			}
		}
		return sum;
	}
	
	//Method used to subtract 2 matrices
	public static SquareMatrix subtract(SquareMatrix sm1, SquareMatrix sm2) throws Exception{
		double[][] elements = new double[sm1.matrix.length][sm1.matrix[0].length];
		SquareMatrix subtraction = new SquareMatrix(elements);
		for (int i=0;i<sm1.matrix.length;i++){
			for (int j=0;j<sm1.matrix[0].length;j++){
				elements[i][j] = sm1.matrix[i][j] - sm2.matrix[i][j];
			}
		}
		return subtraction;
	}
	
	//Method used to multiply 2 matrices
	public static SquareMatrix multiply(SquareMatrix sm1, SquareMatrix sm2) throws Exception{
		double[][] result = new double[sm1.matrix.length][sm1.matrix.length];
		SquareMatrix prod = new SquareMatrix(result);
		for(int i=0; i<sm1.matrix.length;i++){
			for(int j=0; j<sm1.matrix.length;j++){
				double sum = 0;
				for(int k=0; k<sm1.matrix.length;k++){
					sum +=sm1.matrix[i][k] * sm2.matrix[k][j];
				}
				result[i][j] = sum;
			}
		}
		return prod;	
	}
	
	//Static versions of above methods
	SquareMatrix unitMatrix(SquareMatrix m1){
		return unitMatrix(m1);
	}
	
	public SquareMatrix add(SquareMatrix m1) throws Exception{
		return add(this,m1);
	}
	
	public SquareMatrix subtract(SquareMatrix m1) throws Exception{
		return subtract(this,m1);
	}
	
	public SquareMatrix multiply(SquareMatrix m1) throws Exception{
		return multiply(this,m1);
	}
	
}
