package module5;

public class TestSquareMatrix {

	public static void main(String[] args) throws Exception {
		
		//Creating matrices needed and printing them out
		double[][] elementsA = {{2.0,0.0,-1.0},{0.0,2.0,0.0},{3.0,0.0,1.0}};
		SquareMatrix A = new SquareMatrix(elementsA);
		double[][] elementsB = {{-1.0,0.0,1.0},{0.0,1.0,0.0},{-3.0,0.0,1.0}};
		SquareMatrix B = new SquareMatrix(elementsB);
		double[][] elementsC = {{2.0,3.0},{3.0,4.0}};
		SquareMatrix C = new SquareMatrix(elementsC);
		double[][] elementsD = {{-4.0,3.0},{3.0,-2.0}};
		SquareMatrix D = new SquareMatrix(elementsD);

		System.out.println("Matrix A: "+A);
		System.out.println("Matrix B: "+B);
		System.out.println("Matrix C: "+C);
		System.out.println("Matrix D: "+D);

		//Checking if all the methods work correctly
		System.out.println("A + B = " + SquareMatrix.add(A,B));
		System.out.println("A - B = " + SquareMatrix.subtract(A,B));
		System.out.println("Commutator is: " + SquareMatrix.subtract(SquareMatrix.multiply(A,B),SquareMatrix.multiply(B,A)));
		System.out.println("C*D = " + SquareMatrix.multiply(C,D));
		if (SquareMatrix.multiply(C,D).equals(SquareMatrix.unitMatrix(2))){
			System.out.println("The result is unitMatrix as expected.");
		}
		else{
			System.out.println("The result is incorrect!");
		}
	}
	

}
