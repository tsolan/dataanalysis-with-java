package module1;

import java.lang.Math;


public class VectorMethods {

	public static void main(String[] args) {
		VectorMethods vm = new VectorMethods();
		double x = 6;
		double y = 3;
		double z = 1; //defining parameters of the 1st vector
		
		double a = 2;
		double b = 4;
		double c =2; //defining parameters of the 2nd vector
		
		double ang = vm.angle(x, y, z, a, b, c);
		System.out.println(ang);
		System.out.println("We get 38.5 degrees as expected, which means that all methods work properly, since we use magnitude and dotProduct in"
				+ " angle method.");
		
		double x1 = 6;
		double y1 = 3;
		double z1 = 1;
		
		double a1 = 0;
		double b1 = 0;
		double c1 =0;
		
		double ang1 = vm.angle(x1, y1, z1, a1, b1, c1);
		System.out.println();
		System.out.println(ang1);
		System.out.println("There is no answer obtained, because the second vector is a zero-vector, which has 0 magnitude."
				+ " So we have division by 0 in angle method.");
		
		

	}
	//Here x,y,z are coordinates of first vector, a,b,c are the coordinates of second vector, dotprod gives the dotProduct
	public double dotProduct (double x, double y, double z, double a, double b, double c){
		double dotprod = x*a + y*b + z*c; //just multiplying needed values as usual
		return dotprod; //returning obtained value
	}
	//Here x,y,z are coordinates of a vector, mag is its magnitude
	public double magnitude (double x, double y, double z){
		double mag = Math.sqrt(x*x + y*y + z*z); //using square root function of Math module to obtained the magnitude from squared value
		return mag;
	}
	
	//Here x,y,z are coordinates of first vector, a,b,c are the coordinates of second vector, angle is the angle between them
	public double angle (double x, double y, double z, double a, double b, double c){
		double dotp = dotProduct(x,y,z,a,b,c); //obtain a dot product of these vectors
		double vector1_mag = magnitude(x,y,z); //obtain magnitude of 1st vector
		double vector2_mag = magnitude(a,b,c); //obtain magnitude of 2nd vector
		
		double cosine = dotp/(vector1_mag*vector2_mag);  //getting cosine by division, since a*b = |a||b|cos(alpha) by definition
		double angle = Math.toDegrees(Math.acos(cosine)); //finding arccosine using Math function and 
		//converting the value from radians to degrees for convinience.
		
		return angle;
	}
	
	
}
