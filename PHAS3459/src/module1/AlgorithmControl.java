package module1;

public class AlgorithmControl {

	public static void main(String[] args) {
		
		AlgorithmControl ac = new AlgorithmControl();
		System.out.println("Printing out numbers from 1 to 20:");
		ac.loop();
		System.out.println("Printing out numbers from 8 to -10:");
		ac.decrement();
		System.out.println("Printing out all values from 1.2 to 10.8 with increment 1.4:");
		ac.increment();
		
		System.out.println("Printing number of loops every 500 loops for 10 seconds:");
        long loop1 = ac.timer(10000, 500);
        System.out.println(loop1);
		System.out.println("Printing number of loops every 50000 loops for 10 seconds:");
        long loop2 =ac.timer(10000, 50000);
        System.out.println(loop2);
        
        System.out.println("Total number of loops for the first one:"+loop1);
        System.out.println("Total number of loops for the second one:"+loop2);
        System.out.println("This difference in numbers is probably due to for the first one each loop has to print out a value much more often"
        		+ "then for the second one. Therefore, each iteration took more time for the first loop and total number is bigger for the second");
        




		
		}
	
	
	public void loop() {
		for(int i = 1;i<=20;i++)
			System.out.println(i);
	}
	
	public void decrement() {
		int x = 8;
		while (x>=-10) {
			System.out.println(x);
			x=x-1; }
		}
	
// method, printing out all values from 1.2 to 10.8 with increment 1.4	
	public void increment()	{  
 		for(double i = 1.2;i<=10.8;i=i+0.2)
 			System.out.println(i);
 	}  

		

	
// method, returning number of loops count for the specified amount of time
	public int timer(long maxTime, int loopSteps){
		long timeNow = System.currentTimeMillis();
		long finishTime = timeNow + maxTime;
		int loops = 0;
		while (System.currentTimeMillis()<=finishTime){
			loops=loops+1;
			int iremainder = loops % loopSteps;
			if (iremainder ==0){
				System.out.println(loops);}
			
		}

		return loops;
	}

}
