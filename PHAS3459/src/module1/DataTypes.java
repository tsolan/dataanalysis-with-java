package module1;

public class DataTypes {

	public static void main(String[] args) {
	// Creating new variables and printing them out:
		double doubVar = 20.0;
		System.out.println("doubVar value is: " + doubVar);
		
		float floatVar = 20.0f;
		System.out.println("floatVar value is: " + floatVar);
		
		int intVar = 20;
		System.out.println("intVar value is: "  + intVar);
		
		long longVar = 20;
		System.out.println("longVar value is: " + longVar);
		
		byte byteVar = 20;
		System.out.println("byteVar value is: " + byteVar);
		
	//Multiplying values by themselves in this part of the code:
		System.out.println();
		System.out.println("doubVar multiplied value is: " + doubVar*doubVar);
		System.out.println("floatVar multiplied value is: " + floatVar*floatVar);
		System.out.println("intVar multiplied value is: " + intVar*intVar);
		System.out.println("longVar multiplied value is: " + longVar*longVar);
		System.out.println("byteVar multiplied value is: " + byteVar*byteVar);
		
	//Investigation the behaviour of mixing types together:
		//First example
		System.out.println();
		System.out.println("Add all Vars together: "+ doubVar+floatVar+intVar+longVar+byteVar);
		System.out.println("As we can see all the used types can be added together and we get the correct value."
				+ " This is explained by that all of them represent a numerical value.");
		System.out.println("Notice that the obtained value is double, since this is the most general data type used.");
		
		//Second example
		System.out.println();
		char charVar = 'a'+12;
		System.out.println("charVar value is: " + charVar);
		System.out.println("Obtained value is the letter in english alphabeth, which comes 12 letter after a(the 1st one).");
		
		//Third example
		System.out.println();
		System.out.println("Adding char and int: "+(charVar+intVar));
		System.out.println("Here we get a weird number! Let's investigate it by obtaining integer value of char variable: ");
		System.out.println(Integer.valueOf(charVar));
		System.out.println("We got 109 as a value. This is due to it returns the int value that the specified Unicode character represents!");
		
	//Using variables, which value is not yet initialised:
		int j=1;
		int i; //As we can see, it is possible to create a variable without assigning value to it.
		//j=j+i; //However, if we try to use it, compilation problem is raised, which says that the variable i is not yet initialised
		
	//Investigation of double to int cast:
		double x = 12.99;
		int y;
		y = (int) x;
		System.out.println();
		System.out.println("Our double value: " + x);
		System.out.println("The result of casting double to int: "+y);
		System.out.println("As it can be seen, the precision is significantly lost. This is due to converting to int does not take the closest"
				+ "value, but simply get rid of everything after decimal point.");
		
		
		

	}

}
