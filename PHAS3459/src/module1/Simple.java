package module1;

import java.util.Date;


public class Simple {

	public static void main(String[] args) {
		System.out.println("Program starting");
		Date myDate = new Date();
		System.out.println(myDate);
		//System.out.println("Program finished")
		//Without semicolon we get an exception -syntax error, 
		//this is due to the statement is not shown to be completed, which is done by semicolon in Java
		
		//fmdlkandkjfanka;
		//This is also Syntax error, due to Java cannot resolve the meaning of it
		System.out.println("Program finished");

	}

}

//Date myDate = new Date();
//Arises a compilation problem due to the code written outside of class




